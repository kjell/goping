package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"time"

	"github.com/gizak/termui"
	"github.com/paulstuart/ping"
)

const (
	debug = false
)

var data []int
var s1 termui.Sparkline
var s2 termui.Sparklines
var pingErrors int

func main() {
	pingErrors = 0
	if len(os.Args) != 2 {
		usage()
		os.Exit(0)
	}
	data = make([]int, 0, 100)
	err := termui.Init()
	if err != nil {
		panic(err)
	}
	defer termui.Close()
	s1 = termui.NewSparkline()
	go pinger(os.Args[1], 10)
	termui.UseTheme("helloworld")
	<-termui.EventCh()

}

func pinger(remote string, timeout int) error {
	for {
		durration, err := sysPinger(remote, timeout)

		if err != nil {
			debugprint(fmt.Sprintf("could not ping ", os.Args[1]))
			debugprint(fmt.Sprintf("%s", err))
			pingErrors++
			durration = 0
		}
		update(int(durration), s1)

		time.Sleep(1 * time.Millisecond * 250)

	}
	return nil
}

func pingPinger(remote string, timeout int) error {
	err := ping.Pinger(os.Args[1], 10)
	return err
}

func sysPinger(remote string, timeout int) (durration float64, err error) {
	out, err := exec.Command("ping", remote, "-c 1 -W 1 -w 1").Output()
	if err != nil {
		return 0, err
	}

	// parse pings output. Get actual duration for the ping.

	// Expect something like:
	/*
		PING 192.168.10.101 (192.168.10.101) 56(84) bytes of data.
		64 bytes from 192.168.10.101: icmp_seq=1 ttl=63 time=12.2 ms

		--- 192.168.10.101 ping statistics ---
		1 packets transmitted, 1 received, 0% packet loss, time 0ms
		rtt min/avg/max/mdev = 12.243/12.243/12.243/0.000 ms
	*/

	// TODO: Handle packetloss.
	/*
		PING vg.no (195.88.54.16) 56(84) bytes of data.

		--- vg.no ping statistics ---
		1 packets transmitted, 0 received, 100% packet loss, time 0ms
	*/

	// And pingreqs bigger than 99ms:
	/*
		PING nike.com (146.197.184.5) 56(84) bytes of data.
		64 bytes from www.Nikefieldhouse.com (146.197.184.5): icmp_seq=1 ttl=239 time=123 ms
	*/

	matches := regexp.MustCompile(`time=(\d+\.\d+)`).FindStringSubmatch(string(out))
	if matches != nil {
		// matches containg :
		// [time=12.2 12.2]
		// get 12.2 and make a float of it..
		t := regexp.MustCompile(`\d+\.\d+`).FindString(matches[0])
		durration, err = strconv.ParseFloat(t, 32)

		if err != nil {
			debugprint(fmt.Sprintf("Could not parse string:%s", matches[0]))
		}
	} else {
		debugprint(fmt.Sprintf("Could not parse ping result:%s", out))
		err = errors.New("Could not parse ping results")
	}
	return
}

func usage() {
	fmt.Printf("Uage: %s <dst>\n", os.Args[0])
}

func update(p int, s1 termui.Sparkline) {
	data = append(data, p)
	if len(data) > 60 {
		s1.Data = data[len(data)-60:]
	} else {
		s1.Data = data[:]
	}
	s1.Title = fmt.Sprintf("%v - %vms", len(data), p)
	s1.Height = 18
	s1.LineColor = termui.ColorYellow

	spls2 := termui.NewSparklines(s1)
	spls2.Height = 21
	spls2.Width = 60
	spls2.Border.FgColor = termui.ColorCyan
	spls2.X = 10
	spls2.Border.Label = "pingtime"

	status := fmt.Sprintf("Missed ping attempts: %d", pingErrors)
	par0 := termui.NewPar(status)
	par0.Height = 1
	par0.Width = 30
	par0.Y = 22
	par0.HasBorder = false

	termui.Render(spls2, par0)
}

func debugprint(s string) {
	if debug {
		fmt.Println(s)
	}
}
