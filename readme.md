# goping

![Screenshot](/goping.png)

fancy pingtool

## To build:

```
go build

# and run it
./goping <ip>

./goping 192.168.10.101
```


## todos
```
[ ] store data in a fifo queue
[ ] print stats
[ ] make app not crash
[ ] change time between pings on commandline
[ ] gaugebar for max last x pings
[ ] sysPinger() - regexp matching pingtime. add tests.
... more??
```